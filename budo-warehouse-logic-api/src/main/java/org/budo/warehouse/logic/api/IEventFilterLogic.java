package org.budo.warehouse.logic.api;

import org.budo.warehouse.service.entity.Pipeline;

/**
 * @author lmw
 */
public interface IEventFilterLogic {
    /**
     * 事件过滤
     * 
     * @see com.alibaba.otter.canal.filter.CanalEventFilter
     * @return 返回true代表通过
     */
    boolean filter(Pipeline pipeline, DataEntry dataEntry);
}