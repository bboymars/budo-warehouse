package org.budo.warehouse.logic.consumer.jdbc;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author limingwei
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SqlUnit {
    private String sql;

    private Object[] parameters;
}