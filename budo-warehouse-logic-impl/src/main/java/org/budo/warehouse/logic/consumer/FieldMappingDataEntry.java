package org.budo.warehouse.logic.consumer;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.budo.support.lang.util.MapUtil;
import org.budo.support.spring.expression.util.SpelUtil;
import org.budo.warehouse.logic.api.DataEntry;
import org.budo.warehouse.logic.api.DataEntryWrapper;
import org.budo.warehouse.service.entity.FieldMapping;
import org.budo.warehouse.service.entity.Pipeline;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * 加入动态列逻辑
 * 
 * @author lmw
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class FieldMappingDataEntry extends DataEntryWrapper {
    private static final Object BEFORE = new Object(), AFTER = new Object();

    private List<FieldMapping> fieldMappings;

    private Pipeline pipeline;

    public FieldMappingDataEntry(DataEntry dataEntry, List<FieldMapping> fieldMappings, Pipeline pipeline) {
        this(fieldMappings, pipeline);
        this.setDataEntry(dataEntry);
    }

    @Override
    public Integer getColumnCount(Integer rowIndex) {
        if (!this.originalFields()) {
            return this.getFieldMappings().size();
        }

        return this.getDataEntry().getColumnCount(rowIndex) + this.getFieldMappings().size();
    }

    @Override
    public String getColumnName(Integer rowIndex, Integer columnIndex) {
        if (!this.originalFields()) {
            return this.getFieldMappings().get(columnIndex).getFieldName();
        }

        Integer columnCount = this.getDataEntry().getColumnCount(rowIndex);
        if (columnIndex >= columnCount) {
            return this.getFieldMappings().get(columnIndex - columnCount).getFieldName();
        }

        return this.getDataEntry().getColumnName(rowIndex, columnIndex);
    }

    @Override
    public String getColumnValueAfter(Integer rowIndex, Integer columnIndex) {
        if (!this.originalFields()) {
            FieldMapping fieldMapping = this.getFieldMappings().get(columnIndex);
            return this.mergeFieldValue(rowIndex, fieldMapping, AFTER);
        }

        Integer columnCount = this.getDataEntry().getColumnCount(rowIndex);
        if (columnIndex >= columnCount) {
            FieldMapping fieldMapping = this.getFieldMappings().get(columnIndex - columnCount);
            return this.mergeFieldValue(rowIndex, fieldMapping, AFTER);
        }

        return this.getDataEntry().getColumnValueAfter(rowIndex, columnIndex);
    }

    @Override
    public String getColumnValueBefore(Integer rowIndex, Integer columnIndex) {
        if (!this.originalFields()) {
            FieldMapping fieldMapping = this.getFieldMappings().get(columnIndex);
            return this.mergeFieldValue(rowIndex, fieldMapping, BEFORE);
        }

        Integer columnCount = this.getDataEntry().getColumnCount(rowIndex);
        if (columnIndex >= columnCount) {
            FieldMapping fieldMapping = this.getFieldMappings().get(columnIndex - columnCount);
            return this.mergeFieldValue(rowIndex, fieldMapping, BEFORE);
        }

        return this.getDataEntry().getColumnValueBefore(rowIndex, columnIndex);
    }

    @Override
    public Boolean getColumnIsKey(Integer rowIndex, Integer columnIndex) {
        if (!this.originalFields()) {
            String fieldValue = this.getFieldMappings().get(columnIndex).getFieldValue();
            return this.fieldValueIsKey(fieldValue, rowIndex);
        }

        Integer columnCount = this.getDataEntry().getColumnCount(rowIndex);
        if (columnIndex >= columnCount) {
            String fieldValue = this.getFieldMappings().get(columnIndex - columnCount).getFieldValue();
            return this.fieldValueIsKey(fieldValue, rowIndex);
        }

        return this.getDataEntry().getColumnIsKey(rowIndex, columnIndex);
    }

    /**
     * 判断值表达式是不是主键
     */
    private Boolean fieldValueIsKey(String fieldValue, Integer rowIndex) {
        Integer columnCount = this.getDataEntry().getColumnCount(rowIndex);
        for (int columnIndex = 0; columnIndex < columnCount; columnIndex++) {
            Boolean columnIsKey = this.getDataEntry().getColumnIsKey(rowIndex, columnIndex);
            if (!columnIsKey) { // 不是主键
                continue;
            }

            // 找到一个主键
            String columnName = this.getDataEntry().getColumnName(rowIndex, columnIndex);
            if (fieldValue.equals("#{" + columnName + "}")) { // 就是这个
                return true;
            }
        }

        return false;
    }

    /**
     * 是否继承原始列
     */
    private boolean originalFields() {
        return null == this.getPipeline().getOriginalFields() // 默认 true
                || this.getPipeline().getOriginalFields();
    }

    /**
     * 值表达式计算
     */
    private String mergeFieldValue(Integer rowIndex, FieldMapping fieldMapping, Object beforeAfter) {
        String fieldValue = fieldMapping.getFieldValue();

        if (!fieldValue.startsWith("#{")) { // 必须是表达式
            throw new IllegalArgumentException("#25 fieldValue=" + fieldValue + ", fieldMapping=" + fieldMapping);
        }

        Map<String, Object> _row = new HashMap<String, Object>();
        Integer originalColumnCount = this.getDataEntry().getColumnCount(rowIndex);
        for (int columnIndex = 0; columnIndex < originalColumnCount; columnIndex++) {
            String key = this.getDataEntry().getColumnName(rowIndex, columnIndex);

            String value;
            if (BEFORE.equals(beforeAfter)) {
                value = this.getDataEntry().getColumnValueBefore(rowIndex, columnIndex);
            } else if (AFTER.equals(beforeAfter)) {
                value = this.getDataEntry().getColumnValueAfter(rowIndex, columnIndex);
            } else {
                throw new IllegalArgumentException("#165 beforeAfter=" + beforeAfter + ", fieldMapping=" + fieldMapping);
            }

            _row.put(key, value);
        }

        Map<String, Object> map = MapUtil.append(_row, "$row", _row);
        return SpelUtil.merge(fieldValue, map);
    }
}