
-- ----------------------------
-- Table structure for tw_column
-- ----------------------------
DROP TABLE IF EXISTS `tw_column`;
CREATE TABLE `tw_column` (
  `column_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `bind_id` bigint(20) NOT NULL COMMENT '对应一次同步映射的绑定关联ID',
  `from_column` varchar(100) NOT NULL COMMENT '源头数据源表格的字段',
  `to_column` varchar(100) NOT NULL COMMENT '目的地数据源表格的字段',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`column_id`),
  KEY `index_tw_column_bind_id` (`bind_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12200 DEFAULT CHARSET=utf8 COMMENT='同步字段映射表';

-- ----------------------------
-- Table structure for tw_config
-- ----------------------------
DROP TABLE IF EXISTS `tw_config`;
CREATE TABLE `tw_config` (
  `config_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `data_source_id` bigint(20) DEFAULT NULL,
  `origin_schema` varchar(100) DEFAULT NULL COMMENT '原始数据库名称',
  `origin_table` varchar(100) DEFAULT NULL COMMENT '原始表名',
  `params` varchar(1000) DEFAULT NULL COMMENT 'URL参数',
  `type` varchar(50) NOT NULL COMMENT '数据源表类型 source-数据源表 target-目的地表 both-两者兼顾',
  `create_table_sql` text COMMENT '建表语句',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`config_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1955 DEFAULT CHARSET=utf8 COMMENT='配置表';

-- ----------------------------
-- Table structure for tw_data_source
-- ----------------------------
DROP TABLE IF EXISTS `tw_data_source`;
CREATE TABLE `tw_data_source` (
  `data_source_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `db_id` bigint(20) NOT NULL COMMENT '数据源类型ID（mysql, oracle, ES, kafka, ActiveMQ等等）',
  `url` varchar(200) DEFAULT NULL,
  `username` varchar(200) DEFAULT NULL COMMENT '数据库用户名',
  `password` varchar(200) DEFAULT NULL COMMENT '数据库密码',
  `type` varchar(50) NOT NULL COMMENT '数据源类型 source-源头数据源 target-目的数据源 both-两者兼顾',
  `canal_instance` varchar(200) DEFAULT NULL COMMENT 'Canal安装目录下/conf目录下的instance名称',
  `is_local` varchar(10) DEFAULT 'no' COMMENT '是否是本地配置 yes-是 no-不是',
  `enabled` varchar(10) DEFAULT 'enabled' COMMENT '启用-enabled 禁用-disabled',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`data_source_id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8 COMMENT='MYSQL配置表';

-- ----------------------------
-- Table structure for tw_db
-- ----------------------------
DROP TABLE IF EXISTS `tw_db`;
CREATE TABLE `tw_db` (
  `db_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `type` varchar(50) NOT NULL COMMENT '数据库类型',
  `driver` varchar(100) DEFAULT NULL COMMENT '数据库驱动',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`db_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1777778 DEFAULT CHARSET=utf8 COMMENT='同步绑定关系表';

-- ----------------------------
-- Table structure for tw_sync_bind
-- ----------------------------
DROP TABLE IF EXISTS `tw_sync_bind`;
CREATE TABLE `tw_sync_bind` (
  `sync_bind_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `from_config_id` bigint(20) NOT NULL COMMENT '来源表id',
  `to_config_id` bigint(20) NOT NULL COMMENT '去向表id',
  `enabled` varchar(10) DEFAULT 'enabled' COMMENT '启用-enabled 禁用-disabled',
  `sync_bind_regex_id` bigint(20) DEFAULT NULL COMMENT 'regex',
  `privileges` int(10) DEFAULT NULL COMMENT '事件类型权限值',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`sync_bind_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1082 DEFAULT CHARSET=utf8 COMMENT='同步绑定关系表';

-- ----------------------------
-- Table structure for tw_sync_bind_regex
-- ----------------------------
DROP TABLE IF EXISTS `tw_sync_bind_regex`;
CREATE TABLE `tw_sync_bind_regex` (
  `sync_bind_regex_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `uuid` varchar(50) NOT NULL COMMENT '返回客户端的查询token',
  `from_data_source_id` bigint(20) DEFAULT NULL COMMENT '源头数据源ID',
  `to_data_source_id` bigint(20) DEFAULT NULL COMMENT '目标数据源ID',
  `regex` varchar(1000) DEFAULT NULL COMMENT '白名单正则',
  `black_regex` varchar(1000) DEFAULT NULL COMMENT '黑名单正则',
  `sync_history` varchar(10) DEFAULT 'no' COMMENT 'yes-同步历史数据 no-不同步历史数据',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`sync_bind_regex_id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COMMENT='正则配置表';

-- ----------------------------
-- Table structure for tw_sync_bind_regex_log
-- ----------------------------
DROP TABLE IF EXISTS `tw_sync_bind_regex_log`;
CREATE TABLE `tw_sync_bind_regex_log` (
  `sync_bind_regex_log_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `sync_bind_regex_id` bigint(20) DEFAULT NULL COMMENT '正则匹配记录id',
  `desc` varchar(300) DEFAULT NULL COMMENT '描述',
  `status` varchar(10) DEFAULT NULL COMMENT '状态，yes-成功 no-失败',
  `order` int(2) DEFAULT '0' COMMENT '排序标记，0开始，越小排名越靠前',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`sync_bind_regex_log_id`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8 COMMENT='正则配置流程日志表';

-- ----------------------------
-- Table structure for tw_warning
-- ----------------------------
DROP TABLE IF EXISTS `tw_warning`;
CREATE TABLE `tw_warning` (
  `warning_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `data_source_id` bigint(20) NOT NULL COMMENT '数据源ID',
  `filter_regex` varchar(1000) DEFAULT NULL COMMENT '预警过滤正则，多个以逗号隔开，正则规则与canal保持一致，详见 https://github.com/alibaba/canal/wiki/AdminGuide',
  `privileges` int(10) DEFAULT NULL COMMENT '时间类型权限值',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`warning_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COMMENT='预警表';

-- ----------------------------
-- Table structure for tw_warning_detail
-- ----------------------------
DROP TABLE IF EXISTS `tw_warning_detail`;
CREATE TABLE `tw_warning_detail` (
  `warning_detail_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `warning_id` bigint(20) NOT NULL COMMENT '预警表ID',
  `email` varchar(100) NOT NULL COMMENT '邮箱地址',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`warning_detail_id`)
) ENGINE=InnoDB AUTO_INCREMENT=260 DEFAULT CHARSET=utf8 COMMENT='预警邮箱明细';
